﻿using UnityEngine;
using System.Collections;

public class TitleMove : MonoBehaviour {
    public GameObject[] target = new GameObject[3];
    public float EASING = 10;
    public static int PointNo = 0;
    Vector3 diff;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        MoveAnime();
	}

    void MoveAnime()
    {
        //イージング
        diff = target[PointNo].transform.position - this.transform.position;
        

        // 十分近づいたらアニメーション終了
        if (diff.magnitude > 0.5f)
        {
            Vector3 v = diff * EASING;
            this.transform.position += v;
         
        }
    }
}
