﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CountDownScript : MonoBehaviour {

	[SerializeField]
	Sprite[] numberObject = new Sprite[1];
	[SerializeField]
    SpriteRenderer countIage;
    PlayerController player;
    [SerializeField]
    Text countText;
    public SoundController audioMngr;
    
	

	[SerializeField]
	Animator ani;

	// Use this for initialization
	void Start () {
        audioMngr = GameObject.Find("SoundController").GetComponent<SoundController>();
       // player = GameObject.Find("player").GetComponent<PlayerController>();
       // player.enabled = false;
		//StartCoroutine ("CountEvent");
	}

    void OnEnable()
    {
        player = GameObject.Find("player").GetComponent<PlayerController>();
        BgScroll.ScrollFlg = false;
        player.enabled = false;
        audioMngr = GameObject.Find("SoundController").GetComponent<SoundController>();
        StartCoroutine("CountEvent");
    }
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator CountEvent(){
		
		ani.SetTrigger ("next");
        countText.text = "3";
      //  yield return new WaitForSeconds(0.2f);
        audioMngr.CountDownSound();
		yield return new WaitForSeconds (0.7f);
        countText.text = "2";
        audioMngr.CountDownSound();
		ani.SetTrigger ("next");
		yield return new WaitForSeconds (0.7f);
        countText.text = "1";
        audioMngr.CountDownSound();
		ani.SetTrigger ("next");
		yield return new WaitForSeconds (0.7f);
       
        player.enabled = true;
        BgScroll.ScrollFlg = true;
		gameObject.SetActive (false);
	}
}
