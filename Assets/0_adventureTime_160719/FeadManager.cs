﻿using UnityEngine;
using System.Collections;

public class FeadManager : MonoBehaviour {

    public enum FlagState
    {
        None = 0,
        In,
        Out
    }

    public FlagState flagState;
	Texture2D texture;
	public bool flag = true;
    public float cnt = 1;
    public bool ClickFlag = false;
    [SerializeField]
    GameObject countObs;
	
	void Awake()
	{
		texture = new Texture2D(1, 1);
        flagState = FlagState.Out;
        countObs = GameObject.Find("CountDown");
        countObs.SetActive(false);
	}
	
	void OnGUI()
	{
		texture.SetPixel(0, 0, new Color(0, 0, 0, cnt));
		texture.Apply();
		
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), texture);
		
		//フェードアウト
		if (flagState == FlagState.Out)
		{
			cnt -= 0.01f;
            if (cnt < 0)
            {
                countObs.SetActive(true);
                countObs.GetComponent<CountDownScript>().enabled = true;
                flagState = FlagState.None;

            }	
		}else if (flagState == FlagState.In)
        {
            //フェードアウト
            cnt += 0.01f;
            if (cnt > 1) flagState = FlagState.None;

        }
        else if (flagState == FlagState.None)
        {
           
        }
		
	}

   
	void OnApplicationQuit()
	{
		Destroy(texture);
	}

}
