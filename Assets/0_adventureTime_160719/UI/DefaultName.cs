﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DefaultName : MonoBehaviour
{
    string[] KUGIRI = { "\r", "\n" };	//データの区切り文字

    //string[] nameList = new string[1000];
    [SerializeField]
    InputField inputText;
    void Start()
    {
        TextAsset nameTextFile = Resources.Load("text/defaultname", typeof(TextAsset)) as TextAsset;
          /// string[] nameList = nameTextFile.text.Split("\n"[0]);
           string[] nameList = nameTextFile.text.Replace("\r", "").Split("\n"[0]);
        int num=Random.Range(0,nameList.Length);
        inputText.text = nameList[num];
        Debug.Log(nameList[num]);
    }
   

}
