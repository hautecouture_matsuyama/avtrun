﻿using UnityEngine;
using System.Collections;

public class Levels : MonoBehaviour {


	public Camera LevelsCam;
	public string selectedTxt;
	public Renderer backRender;
    public GameObject backRender2;
    public Renderer nextRender;
    public GameObject nextRender2;
	public Texture[] backTexture;
    public Texture[] nextTexture;
	public GameObject levelloadinParent;
	public uiController UIChild;
	public TextMesh loadingLevelName;
    public GameObject levels2,levels3;

	// Use this for initialization
	void Start () {
	
	}
	RaycastHit hitObject;
	Ray rayObjLevels;
	// Update is called once per frame
	void Update () {



		if (Input.GetKeyDown (KeyCode.Mouse0)) {
			selectedTxt="";
			rayObjLevels = LevelsCam.ScreenPointToRay (Input.mousePosition);
			
			if (Physics.Raycast (rayObjLevels, out hitObject)) 
			{
				selectedTxt=hitObject.collider.name;
			}


		
			if(selectedTxt.Contains("Level"))
			{
				SoundController.Static.PlayClickSound();
                backRender.gameObject.GetComponent<BoxCollider>().enabled = false;
                nextRender.gameObject.GetComponent<BoxCollider>().enabled = false;
                iTween.MoveTo(levelloadinParent, iTween.Hash("position", new Vector3(0.27f, 1.5f, 0)));
				loadingLevelName.text=hitObject.collider.name;
				iTween.MoveTo (UIChild.levels,iTween.Hash("position" ,new Vector3(29,0,0)));
                iTween.MoveTo(levels2, iTween.Hash("position", new Vector3(29, 0, 0)));
                iTween.MoveTo(levels3, iTween.Hash("position", new Vector3(29, 0, 0)));
				StartCoroutine(MyLoadLevel());
			//Application.LoadLevel(selectedTxt);
			}
			switch(selectedTxt)
			{
//			case "Level 1":
//				Application.LoadLevel("Level 1");
//				break;
//			case "Level 2":
//				Application.LoadLevel("Level1");
//				break;
//			case "Level 3":
//				Application.LoadLevel("Level1");
//				break;
//			case "Level 4":
//				Application.LoadLevel("Level1");
//				break;
//			case "Level 5":
//				Application.LoadLevel("Level1");
//				break;
//			case "Level 6":
//				Application.LoadLevel("Level1");
//				break;
//			case "Level 7":
//				Application.LoadLevel("Level1");
//				break;
//			case "Level 8":
//				Application.LoadLevel("Level1");
//				break;
//			case "Level 9":
//				Application.LoadLevel("Level1");
//				break;
			case "Back":
                    backRender2.SetActive(true);
				backRender.material.mainTexture=backTexture[1];
				//Application.LoadLevel("Home");
				break;
            case "Back2":
                backRender2.SetActive(true);
                backRender.material.mainTexture = backTexture[1];
                break;
            case "Back3":
                backRender.material.mainTexture = backTexture[1];
                break;
            case "Next":
                nextRender2.SetActive(true);
                nextRender.material.mainTexture = nextTexture[1];
                break;
            case "Next2":
                nextRender.material.mainTexture = nextTexture[1];
                break;



			}

				}
		if (Input.GetKeyUp (KeyCode.Mouse0))
        {
			selectedTxt="";
			rayObjLevels = LevelsCam.ScreenPointToRay (Input.mousePosition);
			
			if (Physics.Raycast (rayObjLevels, out hitObject))
			{
				selectedTxt=hitObject.collider.name;
			}

			//if(selectedTxt.Contains("Level")) Application.LoadLevel(selectedTxt);
			switch(selectedTxt)
			{
			case "Back":
                    UIChild.showFlag = false;
                NendMenyManager.Instance.NendIconView(true);
				SoundController.Static.PlayClickSound();
				backRender.material.mainTexture=backTexture[0];
             //   UIChild.ui.SetActive(true);
                TitleMove.PointNo = 0;
				//iTween.MoveTo(UIChild.ui,new Vector3(0,0,0),2f);
				iTween.MoveTo(UIChild.levels,new Vector3(29,0,0),2f);
                iTween.MoveTo(levels2, new Vector3(29, 0, 0), 2f);
//				if(UIChild.ui.transform.position==new Vector3(29,0,0) && UIChild.creditsDetails.transform.position==new Vector3(0,0,0))
//				{
//					iTween.MoveTo(UIChild.ui,new Vector3(0,0,0),2f);
//					iTween.MoveTo(UIChild.creditsDetails,new Vector3(-29,0,0),1f);
//				}

				break;
            //case "Back1":
            //    SoundController.Static.PlayClickSound();
            //    iTween.MoveTo(UIChild.ui,new Vector3(0,0,0),2f);
            //    iTween.MoveTo(UIChild.creditsDetails,new Vector3(-29,0,0),1f);
            //    break;
            case "Back2":
                SoundController.Static.PlayClickSound();
                backRender2.SetActive(false);
				backRender.material.mainTexture=backTexture[0];
                iTween.MoveTo(UIChild.levels, new Vector3(-0.35f, 0, 0), 2f);
                iTween.MoveTo(levels2, new Vector3(29, 0, 0), 2f);
                break;
            case "Back3":
                SoundController.Static.PlayClickSound();
				backRender.material.mainTexture=backTexture[0];
                iTween.MoveTo(levels2, new Vector3(-0.35f, 0, 0), 2f);
                iTween.MoveTo(levels3, new Vector3(29, 0, 0), 2f);
                break;
            case "Next":
                SoundController.Static.PlayClickSound();
                nextRender2.SetActive(false);
                nextRender.material.mainTexture = nextTexture[0];
                iTween.MoveTo(levels2, new Vector3(-0.35f, 0, 0), 2f);
                iTween.MoveTo(UIChild.levels, new Vector3(29, 0, 0), 2f);
                break;
            case "Next2":
                SoundController.Static.PlayClickSound();
                nextRender2.SetActive(false);
                nextRender.material.mainTexture = nextTexture[0];
                iTween.MoveTo(levels3, new Vector3(-0.35f, 0, 0), 2f);
                iTween.MoveTo(levels2, new Vector3(29, 0, 0), 2f);
                break;

            default:
                backRender2.SetActive(false);
                nextRender2.SetActive(false);
                backRender.material.mainTexture = backTexture[0];
                nextRender.material.mainTexture = nextTexture[0];
                break;
			}
			
		}
		
	
	}
	IEnumerator MyLoadLevel()
	{
     //   NendMenyManager.Instance.AdmobBannerView(false);
		yield return new WaitForSeconds(3f);
		Application.LoadLevel(loadingLevelName.text);
	}
}
