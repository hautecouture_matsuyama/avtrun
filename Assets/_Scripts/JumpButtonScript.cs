﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class JumpButtonScript : MonoBehaviour {

    PlayerController player;
    EventTrigger button;
	// Use this for initialization
	void Start () {
        button = GetComponent<EventTrigger>();
        player = GameObject.Find("player").GetComponent<PlayerController>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerDown;
        entry.callback.AddListener((data) => { OnPointerDownDelegate((PointerEventData)data); });
        button.triggers.Add(entry);


	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnPointerDownDelegate( PointerEventData data )
	{
		 player.OnClickJumpButton();
	}
}

