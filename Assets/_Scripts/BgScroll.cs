﻿using UnityEngine;
using System.Collections;

public class BgScroll : MonoBehaviour {

    public float speed = 0.1f;
    public static bool ScrollFlg=true;
    private Vector2 offset;
    private float x;
    public PlatformerControllerMovement movement;


    void Start()
    {
        ScrollFlg = true;
        movement = GameObject.Find("player").GetComponent<PlatformerControllerMovement>();
    }

    // Update is called once per frame

    void Update()
    {
        if(ScrollFlg==true)
        {
            // 時間によってxの値が0から1に変化していく.1になったら0に戻り繰り返す.
            //float x = Mathf.Repeat(Time.time*speed, 1);

            if (movement.velocity.x != 0)
            {
                x += speed*Time.deltaTime;
                offset = new Vector2(x, 0);
                // マテリアルにオフセットを設定する.
                GetComponent<Renderer>().sharedMaterial.SetTextureOffset("_MainTex", offset);
            }
        }
        
    }

    public void ScrollFlgFalse()
    {
        ScrollFlg = false;
    }

}