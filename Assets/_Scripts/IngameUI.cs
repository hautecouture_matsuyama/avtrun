﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Collections.Generic;
using NendUnityPlugin.AD;


public class IngameUI : MonoBehaviour {
	
	// Use this for initialization
    public Text finalCoins,finalScoreTxt;//finalCoins:リザルト時の総コインカウント
	public Camera mainCam;
	public string hitObjName;
	public int numberofCoins,life=3,CoinsToDisplay,scoreToDisplay;
	public static int scoreCount;
	public TextMesh displyCount,gameOver;
	public Transform target;
	public GameObject levelCompleted,star1,star2,star3,coinCount,lifeCount,pauseButton,GverTryAgain,home,resume,pauseContainer,scoreParent,LevelIndicatorParent,scoreBoardMenu;
	public Renderer nextRender,reloadRender,homeRender;
	public Texture[] nextTexture,reloadTexture,homeTexture;
	public bool i=true;
	public  float percentage;
	public PlayerController playerControllerChild;
	public bool isGameEnded=false;
	public bool isLevelEnd=false;
	string msg;
    public bool PauseFlg=false;
    [SerializeField]
    GameObject coundDownObs;
    [SerializeField]
    public Text lifeText;
    [SerializeField]
   public Text coinCounterText;
    [SerializeField]
    public TextMesh scoreCountTxt;
    [SerializeField]
    Text  MyRank;
    [SerializeField]
    GameObject levelUis;
    [SerializeField]
    Text MyAppleText;
  


    Vector2 min;


	[SerializeField]
	string[] chaina = new string[0];

	[SerializeField]
	string[] thai = new string[0];

    bool onesFlag=false;
    public SoundController audioMngr;
    public levelIndicator levelindicator;

    [SerializeField]
    GameObject playBGM;
    [SerializeField]
    GameObject overBGM;

	void OnEnable()
	{
		CollisionAndTrigger.displayAd += displayerAD;
	 
	}

	void displayerAD(System.Object obj, EventArgs args)
	{
		Debug.Log ("Ad called");
	}

    void Awake()
    {
        DialogManager.Instance.SetLabel("Yes", "No", "Close");
    }

	void Start () {
       
        audioMngr = GameObject.Find("SoundController").GetComponent<SoundController>();
        playBGM = GameObject.Find("play (1)");
        overBGM = GameObject.Find("gameover (1)");
        playBGM.GetComponent<AudioSource>().enabled = true;
        if(!PlayerPrefs.HasKey(Application.loadedLevelName+"Score")){
             PlayerPrefs.SetInt(Application.loadedLevelName+"Score",0);
        }
        levelUis = GameObject.Find("LevelUI");
        MyRank = GameObject.Find("MyRankNum").GetComponent<Text>();

        //RankingManager.Instance._myrankText = MyRank;

        GverTryAgain = GameObject.Find("GameOverPanel").GetComponent<GetChild>().resultObject;
        finalCoins = GameObject.Find("applenum").GetComponent<Text>();
        finalScoreTxt = GameObject.Find("allapplenum").GetComponent<Text>();
       levelCompleted = GameObject.Find("ResultPanel");
        star1 = GameObject.Find("pai1");
        star2 = GameObject.Find("pai2");
        star3 = GameObject.Find("pai3");
        star1.SetActive(false);
        star2.SetActive(false);
        star3.SetActive(false);

        /*再生前に出ているリザルトとゲームオーバーのパネルを非表示にする*/
        levelCompleted.SetActive(false);
        GverTryAgain.SetActive(false);
        /*--------------------------------*/
        coundDownObs = GameObject.Find("CountDown");
        lifeText = GameObject.Find("lifetext").GetComponent<Text>();
        coinCounterText = GameObject.Find("cointext").GetComponent<Text>();
        min = Camera.main.ViewportToWorldPoint(Vector2.zero);
		scoreCount = 0;
        life = 1;
		lifeText.text ="ｘ" + life.ToString ();
        coinCounterText.text = "ｘ" + numberofCoins.ToString();
		isGameEnded = false;
		
	    if (Camera.main.aspect >= 1.5) //3:2
		{
			 //nochange
		}
		else
		{
			mainCam.orthographicSize = 6.5f;
			mainCam.transform.Translate(0,-1,0);
		}
		
	}
	public void  finalCoinsCaliculations()
	{
		Time.timeScale = 1;
		iTween.ValueTo (gameObject, iTween.Hash ("delay",1f,"from",0,"to",numberofCoins,"time",2.0f,"onupdate","onUpdateCount" ,"onstart","playCountingSound","onstarttarget",gameObject ));
		iTween.ValueTo (gameObject, iTween.Hash ("delay",3f,"from",0,"to",scoreCount,"time",2.0f,"onupdate","onUpdateCountforScore","onstart","playCountingSound","onstarttarget",gameObject ));
		
	}
	void playCountingSound()
	{
		SoundController.Static.playcoinCounting ();
	}
	void onUpdateCount(int value)
	{
		CoinsToDisplay = value;
	}
	void onUpdateCountforScore(int value1)
	{
		scoreToDisplay = value1;
		
		
	}
	Ray rayObj;	RaycastHit hitObject;
	// Update is called once per frame
	void Update ()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {

            //
            // YES NO ダイアログ
            DialogManager.Instance.ShowSelectDialog("終了しますか？", (bool result) => { if (result)Application.Quit(); });

        }

		//finalCoins.text="Coins X "+CoinsToDisplay;


		/*if (I2.Loc.LocalizationManager.CurrentLanguage == "Chaina(hantai)") {
			finalScoreTxt.GetComponent<TextMesh>().text = chaina[0]+ scoreToDisplay;
			finalCoins.GetComponent<TextMesh>().text = chaina[1]+CoinsToDisplay;
		}
		if (I2.Loc.LocalizationManager.CurrentLanguage == "Thai") {
			finalScoreTxt.GetComponent<TextMesh>().text = thai[0]+ scoreToDisplay;
			finalCoins.GetComponent<TextMesh>().text = thai[1]+CoinsToDisplay;
		}*/
        //日本語に直す
        finalScoreTxt.GetComponent<Text>().text = PlayerPrefs.GetInt("Cumulative").ToString();
		finalCoins.GetComponent<Text>().text = CoinsToDisplay.ToString();
        
		//finalScoreTxt.text = "Score X  " + scoreToDisplay;
		scoreCountTxt.text = scoreCount.ToString ();
		
		if (Input.GetKeyDown(KeyCode.Mouse0)) 
		{
           
			rayObj = mainCam.ScreenPointToRay(Input.mousePosition);
			if(Physics.Raycast(rayObj,out hitObject))
			{
				hitObjName=hitObject.collider.name;
				switch (hitObjName) 
				{
//				case "resume":
//					pauseContainer.SetActive(false);
//					pauseButton.SetActive(true);
//					Time.timeScale = 1;
//					break;
//				case "pause":
//					if(Time.timeScale == 1)
//					{
//						pauseContainer.SetActive(true);
//						pauseButton.SetActive(false);
//						pauserender.material.mainTexture = pauseTexture [1];
//						Time.timeScale = 0;
//					}
//					else
//					{
//						pauseContainer.SetActive(false);
//						pauserender.material.mainTexture = pauseTexture [0];
//						Time.timeScale = 1;
//					}
//					break;
				case "Next":
					SoundController.Static.PlayClickSound();
					nextRender.material.mainTexture=nextTexture[1];
					//Application.LoadLevel("Level 2");
					break;
				case "PlayAgain":
					SoundController.Static.PlayClickSound();
					reloadRender.material.mainTexture=reloadTexture[1];
					//Application.LoadLevel("Level 1");
					break;
				case "main menu":
					SoundController.Static.PlayClickSound();
					homeRender.material.mainTexture=homeTexture[1];
					//Application.LoadLevel("Home");
					break;
				case "home":
                     
					//Application.LoadLevel("Home");
					break;
                case "pause":
                        levelUis.SetActive(false);
                    PauseFlg = true;
                       
                    break;
					
				}
			}
		}

		if (Input.GetKeyUp(KeyCode.Mouse0)) 
		{
			originalTextures();
			rayObj = mainCam.ScreenPointToRay(Input.mousePosition);
			if(Physics.Raycast(rayObj,out hitObject))
			{
				hitObjName=hitObject.collider.name;

				switch (hitObjName) 
				{
				case "resume":
                        audioMngr.PlayClickSound();
                        levelUis.SetActive(true);
                           NendMenyManager.Instance.NendRec2View(false);
                        PauseFlg = false;
					    pauseContainer.SetActive(false);
					    ActiveIngameUI();
                        StartCoroutine("CountDown");
					    Time.timeScale = 1;
                       
					    break;
				case "pause":
					if(Time.timeScale == 1)
					{          
                            NendMenyManager.Instance.NendRec2View(true);
						pauseContainer.SetActive(true);
						DeactiveIngameUI();
						Time.timeScale = 0;
						//GameObject.FindGameObjectWithTag("ADS").SendMessage("showFullAD",SendMessageOptions.DontRequireReceiver);
					}
					else
					{
						pauseContainer.SetActive(false);
						Time.timeScale = 1;
                        
					}
					break;
				case "Next":
					nextRender.material.mainTexture=nextTexture[0];
					Application.LoadLevel(Application.loadedLevel+1);
					break;
				case "PlayAgain":
                           NendMenyManager.Instance.NendRec2View(false);
					reloadRender.material.mainTexture=reloadTexture[0];
					Time.timeScale=1;
					Application.LoadLevel(Application.loadedLevelName);
					break;
				case "main menu":
                    NendMenyManager.Instance.NendRec2View(false);
					homeRender.material.mainTexture=homeTexture[0];
					Application.LoadLevel("Home");
					break;
				case "home":
                    audioMngr.PlayClickSound();
                           NendMenyManager.Instance.NendRec2View(false);
					Application.LoadLevel("Home");
					break;
                case "share_facebook":
                  //  ShareFacebook();
                    break;
                case "share_line":
                    ShareLINE();
                    break;
                case "share_twitter":
                    ShareTwitter();
                    break;

					
				}
			}
		}

		/*if (Input.GetKeyDown (KeyCode.Escape))
        {
            if (isGameEnded == false)
            {
                if (Time.timeScale == 1)
                {
                    pauseContainer.SetActive(true);
                    DeactiveIngameUI();
                    Time.timeScale = 0;
                }
                else
                {
                    pauseContainer.SetActive(false);
                    ActiveIngameUI();
                    Time.timeScale = 1;
                }



            }

			if (isLevelEnd == true)
            {
				Application.LoadLevel ("Home");
			}

		}*/

	}

    public void OnClickTwitterButton(){
        ShareTwitter();
    }

    public void OnClickFaceBookButton(){
      // ShareFacebook();
    }
    public void OnClickLINEButton(){
        ShareLINE();
    }

    public void PauseButton()
    {
        NendMenyManager.Instance.NendResume();
        if(Time.timeScale == 1)
					{          
                            NendMenyManager.Instance.NendRec2View(true);
						pauseContainer.SetActive(true);
						DeactiveIngameUI();
						Time.timeScale = 0;
						//GameObject.FindGameObjectWithTag("ADS").SendMessage("showFullAD",SendMessageOptions.DontRequireReceiver);
					}
					else
					{
						pauseContainer.SetActive(false);
						Time.timeScale = 1;
                        
					}
    }

    public void OnClickNextLevelButton(){
      NendMenyManager.Instance.NendResultIconView(false);
        NendMenyManager.Instance.NendRecView(false);
        NendMenyManager.Instance.NendRec2View(false);
        NendMenyManager.Instance.NendOverIconView(false);
        	nextRender.material.mainTexture=nextTexture[0];
					Application.LoadLevel(Application.loadedLevel+1);
    }

    public void OnClickRetryButton(){
        NendMenyManager.Instance.NendResultIconView(false);
         NendMenyManager.Instance.NendRecView(false);
         NendMenyManager.Instance.NendRec2View(false);
        NendMenyManager.Instance.NendOverIconView(false);
         NendMenyManager.Instance.AdmobBannerView(false);
            NendMenyManager.Instance.NendBannerView(false);
					reloadRender.material.mainTexture=reloadTexture[0];
					Time.timeScale=1;
					Application.LoadLevel(Application.loadedLevelName);
    }

    public void OnClickHomeButton(){
        NendMenyManager.Instance.NendResultIconView(false);
         NendMenyManager.Instance.NendRecView(false);
          NendMenyManager.Instance.NendRec2View(false);
         NendMenyManager.Instance.NendOverIconView(false);
         NendMenyManager.Instance.AdmobBannerView(false);
            NendMenyManager.Instance.NendBannerView(false);
					Application.LoadLevel("Home");
    }


 IEnumerator CountDown(){
     yield return new WaitForSeconds(0.2f);
     coundDownObs.GetComponent<CountDownScript>().enabled = true;
 }


	public void OnLevelEndEscape()
	{
		isLevelEnd = true;

	}

	public void DeactiveIngameUI()
	{
        levelUis.SetActive(false);
		coinCount.SetActive(false);
		lifeCount.SetActive(false);
		//pauseButton.SetActive(false);
		scoreParent.SetActive (false);
		LevelIndicatorParent.SetActive (false);

	}
	public void ActiveIngameUI()
	{
        levelUis.SetActive(true);
		coinCount.SetActive(true);
		lifeCount.SetActive(true);
		//pauseButton.SetActive(true);
		scoreParent.SetActive (true);
		LevelIndicatorParent.SetActive (true);


		}
    public void OnLevelEnd()
    {

        isGameEnded = true;
        scoreBoardMenu.transform.Translate(0, -10, 0);

        iTween.MoveTo(scoreBoardMenu, iTween.Hash("islocal", true, "position", new Vector3(7.24f, -1.11f, 0), "time", 1f, "delay", 6f, "easetype", iTween.EaseType.easeInOutBack, "onstart", "playCountingSound", "onstarttarget", gameObject, "oncomplete", "OnLevelEndEscape", "oncompletetarget", gameObject));
        Time.timeScale = 0;
       // iTween.MoveTo(levelCompleted, iTween.Hash("islocal", true, "position", new Vector3(min.x, 1.4f, 0), "time", 1f));
		
        levelCompleted.SetActive(true);
        finalCoinsCaliculations();
        DeactiveIngameUI();
        PercentageCaliculation();
        StarsDisplay();

        
        //		GameObject.FindGameObjectWithTag("ADS").SendMessage("showFullAD",SendMessageOptions.DontRequireReceiver);
    }
    public void StarsDisplay()
    {
       
        
        if (numberofCoins < 30)
        {

           if(numberofCoins >= PlayerPrefs.GetInt(Application.loadedLevelName+"Score")) PlayerPrefs.SetInt(Application.loadedLevelName, 1);
            star1.SetActive(true);
            star2.SetActive(false);
            star3.SetActive(false);
          /*  Vector3 OriginalScale = star1.transform.localScale;
            star1.transform.localScale = new Vector3(10, -10, 1);
            iTween.ScaleTo(star1, iTween.Hash("scale", OriginalScale, "time", 0.4f, "easetype", iTween.EaseType.easeInOutBounce, "delay", 0.5f, "onstart", "EnableStar1Render", "onstarttarget", gameObject));
            //print ("50");*/
        }
        else if (numberofCoins < 45 && numberofCoins > 30)
        {

           if(numberofCoins >= PlayerPrefs.GetInt(Application.loadedLevelName+"Score")) PlayerPrefs.SetInt(Application.loadedLevelName, 2);

            star1.SetActive(true);
            star2.SetActive(true);
              star3.SetActive(false);
       /*     Vector3 OriginalScale = star1.transform.localScale;
            star1.transform.localScale = new Vector3(10, -10, 1);

            iTween.ScaleTo(star1, iTween.Hash("scale", OriginalScale, "time", 0.4f, "easetype", iTween.EaseType.easeInOutBounce, "delay", 0.5f, "onstart", "EnableStar1Render", "onstarttarget", gameObject));
            OriginalScale = star2.transform.localScale;
            star2.transform.localScale = new Vector3(10, -10, 1);
            iTween.ScaleTo(star2, iTween.Hash("scale", OriginalScale, "time", 0.4f, "easetype", iTween.EaseType.easeInOutBounce, "delay", 1.0f, "onstart", "EnableStar2Render", "onstarttarget", gameObject));
            //print ("51 and  94");*/
        }
        else if (numberofCoins > 44)
        {

            if(numberofCoins >= PlayerPrefs.GetInt(Application.loadedLevelName+"Score")) PlayerPrefs.SetInt(Application.loadedLevelName, 3);
            star1.SetActive(true);
            star2.SetActive(true);
            star3.SetActive(true);
        /*    Vector3 OriginalScale = star1.transform.localScale;
            star1.transform.localScale = new Vector3(10, -10, 1);
            iTween.ScaleTo(star1, iTween.Hash("scale", OriginalScale, "time", 0.3f, "easetype", iTween.EaseType.easeInOutBounce, "delay", 0.5f, "onstart", "EnableStar1Render", "onstarttarget", gameObject));

            OriginalScale = star2.transform.localScale;
            star2.transform.localScale = new Vector3(10, -10, 1);
            iTween.ScaleTo(star2, iTween.Hash("scale", OriginalScale, "time", 0.3f, "easetype", iTween.EaseType.easeInOutBounce, "delay", 1.0f, "onstart", "EnableStar2Render", "onstarttarget", gameObject));

            OriginalScale = star3.transform.localScale;
            star3.transform.localScale = new Vector3(10, -10, 1);
            iTween.ScaleTo(star3, iTween.Hash("scale", OriginalScale, "time", 0.3f, "easetype", iTween.EaseType.easeInOutBounce, "delay", 1.5f, "onstart", "EnableStar3Render", "onstarttarget", gameObject));
            //print ("95");*/
        }
         Debug.Log("コイン:"+numberofCoins);
      PlayerPrefs.SetInt("Cumulative",PlayerPrefs.GetInt("Cumulative")+numberofCoins);
         Debug.Log("累計コイン:"+PlayerPrefs.GetInt("Cumulative"));


      /*  if (RankingManager.Instance.CheckAccount())
                {*/
                    //スコア送信
                    //nkingManager.Instance.SendScore( PlayerPrefs.GetInt("Cumulative"));

                   //RankingManager.Instance.GetRankingData();
                  

                    StartCoroutine(RankingCheaker());
                    i2 = 0;
               // }
         PlayerPrefs.SetInt(Application.loadedLevelName+"Score",numberofCoins);
    }





     int i2 = 0;
    private IEnumerator RankingCheaker()
    {

        
      /*  //ランキング取得できたら
        while (PlayerPrefs.GetString("MYR") == "" && i2 < 1)
        {
            MyRank.text = "Loading";
            yield return new WaitForSeconds(1f);
        }

        */
        yield return new WaitForSeconds(0.5f);
         
        string myrank = PlayerPrefs.GetString("MYR");
        MyRank.text = myrank;
        i2++;

        PlayerPrefs.SetString("MYR", "");

        if (i2 < 2)
        {
            //RankingManager.Instance.GetRankingData();
            StartCoroutine(RankingCheaker());

          //  RankingManager.Instance.flg = falsetargetSdkVersion
        }
    }






	public void EnableStar1Render()
	{
		SoundController.Static.playStarsSound ();
	//	star1.GetComponent<Renderer>().enabled = true;
        star1.SetActive(true);
	}
	public void EnableStar2Render()
	{
		SoundController.Static.playStarsSound ();
//		star1.GetComponent<Renderer>().enabled = true;
//		star2.GetComponent<Renderer>().enabled = true;
        star1.SetActive(true);
		star2.SetActive(true);
	}
	public void EnableStar3Render()
	{
		SoundController.Static.playStarsSound ();
		//star1.GetComponent<Renderer>().enabled = true;
	//	star3.GetComponent<Renderer>().enabled = true;
          star1.SetActive(true);
          star3.SetActive(true);
	}
	public void PercentageCaliculation()
	{
		float temp=100*numberofCoins;
		percentage=temp/CoinRotation.totalCoinCount;

	}
	public void OnGameOver()
	{
		if (life < 0) 
		{
            playBGM.GetComponent<AudioSource>().enabled = false;
            overBGM.GetComponent<AudioSource>().enabled = true;
            Time.timeScale=0;
            levelUis.SetActive(false);
			DeactiveIngameUI();
			//AdmobManager.ShowInterstitial();
			//VpadnManager.ShowInterstitial();
			GverTryAgain.SetActive(true);
            NendMenyManager.Instance.NendResume();
            NendMenyManager.Instance.NendBannerView(true);
             NendMenyManager.Instance.NendRecView(true);
            NendMenyManager.Instance.NendOverIconView(true);
			GameObject.FindGameObjectWithTag("PlayerRender").GetComponent<MeshRenderer>().enabled=false;
			
			OnLifeCountZero();
			//gameObject.GetComponent<playerControl>().enabled=false;
		//	GameObject.FindGameObjectWithTag("ADS").SendMessage("showFullAD",SendMessageOptions.DontRequireReceiver);
		}

	}

    void ShowInterstitial(){
        NendAdInterstitial.Instance.Show();
    }

	public void originalTextures()
	{
		reloadRender.material.mainTexture=reloadTexture[0];
		homeRender.material.mainTexture=homeTexture[0];
		nextRender.material.mainTexture=nextTexture[0];
	}
	public void OnLifeCountZero()
	{
		//playerControllerChild.movement.runSpeed = 0;
		//playerControllerChild.movement.walkSpeed = 0;
		//gameObject.GetComponent<PlayerController>().enabled=false;
		//playerScript.SetActive = false;
	}

    public Texture2D shareTexture;

    public void ShareTwitter()
    {
        int score = scoreToDisplay;
        int coinscore = CoinsToDisplay;

     
      //  Application.OpenURL("http://twitter.com/intent/tweet?text=" + WWW.EscapeURL("森の中で" + coinscore + " こリンゴをゲットしたよ！\n　みんなはどれだけ採れるかな？" + "https://play.google.com/store/apps/details?id=com.hautecouture.changeofpace&hl=ja"+"\n #businessfish"));
        UM_ShareUtility.TwitterShare("森の中で" + coinscore + " こリンゴをゲットしたよ！\n　みんなはどれだけ採れるかな？" + "https://play.google.com/store/apps/details?id=com.hautecouture.adventure&hl=ja" + "\n  #AdventureTime", shareTexture);
    }

    public void ShareLINE()
    {
        int score = scoreToDisplay;
        int coinscore = CoinsToDisplay;

        string msg = "森の中で" + coinscore + " こリンゴをゲットしたよ！\n　みんなはどれだけ採れるかな？" + "https://play.google.com/store/apps/details?id=com.hautecouture.adventure&hl=ja";
        string url = "http://line.me/R/msg/text/?" + Uri.EscapeUriString(msg);
        Application.OpenURL(url);
    }

/*    public void ShareFacebook()
    {
        if (!FB.IsInitialized)
        {
            FB.Init(Login, null, null);
            return;
        }
        else
        {
            Login();
        }
    }

    void Login()
    {
        if (!FB.IsLoggedIn)
        {
            FB.Login("", LoginCallback);
            return;
        }
        else
        {
            LoginCallback();
        }
    }

	string FeedLink = "https://www.mymaji.com/#/";
    string FeedLinkName = "";
    string FeedLinkDescription = "";
    string FeedPicture = "";
    private Dictionary<string, string[]> FeedProperties = new Dictionary<string, string[]>();

    void LoginCallback(FBResult result = null)
    {
        if (FB.IsLoggedIn)
        {
            int score = scoreToDisplay;
            int coinscore = CoinsToDisplay;
				FeedLinkName = "Change of Pace";
				FeedLinkDescription ="森の中で" + coinscore + " こリンゴをゲットしたよ！\n　みんなはどれだけ採れるかな？";
				
				FB.Feed(
					link: FeedLink,
					linkName: FeedLinkName,
					linkDescription: FeedLinkDescription,
					picture: "http://100apps.s3.amazonaws.com/000_Original/ShareImage/Change_header.jpg",
					properties: FeedProperties
					);
			   
        }
    }*/
	
}
