﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreIcon : MonoBehaviour {

    [SerializeField]
    private IngameUI Flg;

	// Use this for initialization
	void Start ()
    {
        
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if(GameObject.Find("PauseContainer") || Flg.isGameEnded)
        {
            gameObject.GetComponent<Image>().enabled = false;
        }
        else
        {
            gameObject.GetComponent<Image>().enabled = true;
        }
	}
}
