﻿using UnityEngine;
using System.Collections;

public class SoundController : MonoBehaviour {

	// Use this for initialization

	public AudioClip  clickSound,singleJumpSound;
	public AudioClip coinHitSound;
	public AudioClip StarsSound; 
	public AudioClip coinCounting;
    public AudioClip countSound;
    public AudioClip fallSound; 
	public static SoundController Static ;
	public AudioSource[]  audioSources;
	void Start () {
		Static = this;
	}
	
	// Update is called once per frame

	//for single Jump
	public void PlaySingleJumpSound()
	{
		
		swithAudioSources(singleJumpSound);
	}
	 
	//---------------------------------

	public void PlayClickSound()
	{
		 
		swithAudioSources(clickSound);
	}
	 

	public void playcoinCounting()
	{
		
		GetComponent<AudioSource>().PlayOneShot(coinCounting);
	}

		public void playCoinHit()
	{
		
		GetComponent<AudioSource>().PlayOneShot(coinHitSound);
	}

	public void playStarsSound()
	{
		
		GetComponent<AudioSource>().PlayOneShot(StarsSound);
	}
    public void CountDownSound()
    {
        GetComponent<AudioSource>().PlayOneShot(countSound);
    }

    public void FallSound()
    {
        GetComponent<AudioSource>().PlayOneShot(fallSound);
    }

	void swithAudioSources( AudioClip clip)
	{
		if(audioSources[0].isPlaying)
		{
			audioSources[1].PlayOneShot(clip);
		}
		else audioSources[0].PlayOneShot(clip);

	}
}
