﻿using UnityEngine;
using System.Collections;

public class cameraMoveMent : MonoBehaviour {

	// Use this for initialization

	public Vector3 offset ;
	Transform targetTransform ;
    PlayerController player;
    PlatformerControllerMovement playervel;
    public GameObject target;
    public float EASING = 10;
    Vector3 diff;
    void Start()
    {
        
        //プレイヤー座標をCameraが持つ
        targetTransform = GameObject.Find("player").GetComponent<Transform>();
        playervel = GameObject.Find("player").GetComponent<PlatformerControllerMovement>();
        player = GameObject.Find("player").GetComponent<PlayerController>();
        target = player.gameObject;
    }
	
	// Update is called once per frame
	void Update () {
        if (playervel.velocity.x != 0 && BgScroll.ScrollFlg)
        {
            MoveAnime();
        }
		//transform.position = new Vector3( targetTransform.position.x + offset.x,transform.position.y,transform.position.z  );
         MoveAnime();
	}


        void MoveAnime()
    {
        //イージング
        diff = new Vector3(target.transform.position.x + offset.x, target.transform.position.y, target.transform.position.z) - this.transform.position;
        

        // 十分近づいたらアニメーション終了
        if (diff.magnitude > 0.5f)
        {
            Vector3 v = diff * EASING *Time.deltaTime;
            this.transform.position += new Vector3(v.x, 0, 0);
         
        }
    }
}
