﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using NendUnityPlugin.AD;

public class CollisionAndTrigger : MonoBehaviour {

	// Use this for initialization


	public GameObject ScorePrefab;
	public IngameUI IngameUIChild;
	public static EventHandler displayAd; 
	public PlayerController playerControllerChild;
    public BgScroll scrollflg;
    public static bool moveFlag = false;
    bool lifeCountFlag = false;
    [SerializeField]
    GameObject levelUIs;
    [SerializeField]
    GameObject countObs;
    FeadManager feadManager;
    Vector3 hitPosition;
    PlayerController player;
    public SoundController audioMngr;
    BoxCollider boxcoll;
    bool onesFlag = false;
    GameObject resObs;


    void Start()
    {
        resObs = GameObject.Find("safePoint");
        audioMngr = GameObject.Find("SoundController").GetComponent<SoundController>();
        NendMenyManager.Instance.AdmobBannerView(false);
        NendMenyManager.Instance.NendBannerView(false);
        NendMenyManager.Instance.NendRecView(false);
       
        player = gameObject.GetComponent<PlayerController>();
        feadManager = GameObject.Find("Main Camera").GetComponent<FeadManager>();
        countObs = GameObject.Find("CountDown");
        levelUIs = GameObject.Find("LevelUI");
        IngameUIChild = GameObject.FindGameObjectWithTag("uiCam").GetComponent<IngameUI>();
        scrollflg = GameObject.Find("bg (1)").GetComponent<BgScroll>();
       
       
    }

    void Update()
    {
        if (transform.position.y <= -9 && !onesFlag)
        {

            playerControllerChild.moveFlag = false;
            if (!lifeCountFlag)
            {
                player.enabled = false;
                IngameUIChild.life--;
                IngameUIChild.lifeText.text = "ｘ" + IngameUIChild.life.ToString();
                lifeCountFlag = true;
            }
            playerControllerChild.enabled = false;
            levelUIs.SetActive(false);
            audioMngr.FallSound();
           
            BgScroll.ScrollFlg = false;
           
            if (IngameUIChild.life < 0)
            {
                if (UnityEngine.Random.value < 0.33f)
                {

                    //接続されているときの処理
                    Invoke("ShowInterstitial", 0);

                }
                IngameUIChild.OnGameOver();
                //  if (Random.value < 0.33f)
                //     {

                //接続されているときの処理
                // Invoke("ShowInterstitial", Random.Range(0.9f, 1.2f));

                // }
                //gameObject.GetComponent<playerControl>().enabled=false;
            }
            else
            {
                feadManager.cnt = 0;
                feadManager.flagState = FeadManager.FlagState.In;
                //hit.gameObject.GetComponent<PlayerHurt>().Update();
                hitPosition = resObs.transform.position;
                Invoke("DeadEvent", 1);

                Debug.Log("HIT");



            }
          

            onesFlag = true;
        }
    }
  
	void OnTriggerEnter(Collider incoming)
	{
        //コインに当たったら
        if (incoming.GetComponent<Collider>().name.Contains("coin"))
		{
			incoming.gameObject.SetActive(false);
			IngameUIChild.numberofCoins++;
			IngameUI.scoreCount=IngameUI.scoreCount+100;
			GameObject ScoreIndicator=Instantiate(ScorePrefab,incoming.transform.position,Quaternion.identity)as GameObject;
			
		}
		IngameUIChild.displyCount.text = IngameUIChild.numberofCoins.ToString ();
        
	}

    //動いてる時に当たったら
	void OnControllerColliderHit(ControllerColliderHit hit) 
	{
    


       /* if (hit.collider.name.Contains("coin"))
		{
			hit.gameObject.SetActive(false);
			IngameUIChild.numberofCoins++;
            IngameUI.scoreCount = IngameUI.scoreCount + 100;
			GameObject ScoreIndicator=Instantiate(ScorePrefab,hit.collider.transform.position,Quaternion.identity)as GameObject;

				}
		IngameUIChild.displyCount.text = IngameUIChild.numberofCoins.ToString ();*/

        //リスポーン
	/*	if (hit.collider.name.Contains ("respawn")) 
		{
            playerControllerChild.enabled = false;
            levelUIs.SetActive(false);
            audioMngr.FallSound();
           boxcoll = hit.gameObject.GetComponent<BoxCollider>();
            BgScroll.ScrollFlg = false;
            if (!lifeCountFlag)
            {
                player.enabled = false;
                IngameUIChild.life--;
                lifeCountFlag = true;
            }
			if (IngameUIChild.life < 0) 
			{
              if (UnityEngine.Random.value < 0.33f)
                {

                    //接続されているときの処理
                    Invoke("ShowInterstitial",0);

                }
				IngameUIChild.OnGameOver();
              //  if (Random.value < 0.33f)
           //     {

                    //接続されているときの処理
                   // Invoke("ShowInterstitial", Random.Range(0.9f, 1.2f));

               // }
				//gameObject.GetComponent<playerControl>().enabled=false;
			}
		 	else{
                feadManager.cnt = 0;
                feadManager.flagState = FeadManager.FlagState.In;
				//hit.gameObject.GetComponent<PlayerHurt>().Update();
                hitPosition = hit.collider.transform.GetChild(0).transform.position;
                Invoke("DeadEvent", 1);
              
                Debug.Log("HIT");
              
				
               
			}
            boxcoll.enabled = false;
			 

		}*/

		IngameUIChild.lifeText.text ="ｘ" + IngameUIChild.life.ToString ();
        IngameUIChild.coinCounterText.text = "ｘ" + IngameUIChild.numberofCoins.ToString();

        //クリア
		if (hit.collider.name.Contains ("final")) 
		{
            if (UnityEngine.Random.value < 0.33f)
            {

                //接続されているときの処理
                Invoke("ShowInterstitial", UnityEngine.Random.Range(0.9f, 1.2f));

            }
            levelUIs.SetActive(false);
            NendMenyManager.Instance.NendRecView(true);
            NendMenyManager.Instance.NendResultIconView(true);
         //   if (Random.value < 0.33f)
          //  {

                //接続されているときの処理
               // Invoke("ShowInterstitial", Random.Range(0.9f, 1.2f));

          //  }
    
            Debug.Log("isFinish");
            moveFlag = true;
            scrollflg.ScrollFlgFalse();
			hit.collider.name="END";
			gameObject.SendMessage("StopPlayerAnimation",SendMessageOptions.DontRequireReceiver);

			IngameUIChild.OnLevelEnd();

		  if(displayAd != null)	displayAd(null,null);

		}

	}

 

    void DeadEvent()
    {
      
        transform.position = hitPosition;
       
        lifeCountFlag = false;
        Invoke("ResponEvent",1);
       
    }


    void ResponEvent()
    {
        playerControllerChild.moveFlag = true;
        onesFlag = false;
        feadManager.cnt = 1;
        feadManager.flagState = FeadManager.FlagState.Out;
        levelUIs.SetActive(true);
        
        
    }

    //全面広告はここに！！！！！！！！！
    void ShowInterstitial(){
            NendAdInterstitial.Instance.Show();
        
    }
}
