﻿using UnityEngine;
using System.Collections;

public class dontDestroy : MonoBehaviour {

    static dontDestroy STATIC;

	// Use this for initialization
	void Start () {
        STATIC = gameObject.GetComponent<dontDestroy>();
		DontDestroyOnLoad (gameObject);
	}

    void OnLevelWasLoaded()
    {
        if (GameObject.Find("bgSound") &&
            STATIC != null &&
            STATIC != this)
        {
            Destroy(gameObject);
        }
    }
}
