﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SliderScript : MonoBehaviour
{

    private Slider slider;
    private float level;
    [SerializeField]
    PlatformerControllerMovement playerSpeed;
    [SerializeField]
    PlatformerControllerJumping playerJump;

    void Start()
    {
        playerSpeed = GameObject.Find("player").GetComponent<PlatformerControllerMovement>();
        playerJump = GameObject.Find("player").GetComponent<PlatformerControllerJumping>();
        slider = GetComponent<Slider>();
        slider.value = 12;
        level = slider.value;
    }


    public void MoveSlider()
    {
        playerSpeed.walkSpeed = slider.value;
        playerJump.height = slider.value / 3;
    }
    
}