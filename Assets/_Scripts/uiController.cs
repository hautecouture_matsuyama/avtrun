﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using NendUnityPlugin.AD;

public class uiController : MonoBehaviour {

	// Use this for initialization
	public Camera uiCam;
	public string hitObjName;
    public Text MyRank;
    public Text Mypoint;
	public GameObject playGame,more,review,levels,ui,exit,creditsDetails;
	public Renderer playRender,moreRender,reviewRender,exitRender;
    public GameObject Content;
    [SerializeField]
    GameObject rankingPanel;
	//public tweens Title;
    [SerializeField]
    GameObject[] buttonsUI = new GameObject[0];

 public  Button rankbutton;

    [SerializeField]
    Text MyAppleText;
    [SerializeField]
    GameObject endPanel;
    bool dialogflag=false;
   public bool showFlag = false;
   bool bannerFlag = false;

   void OnLevelWasLoaded()
   {
       //RankingManager.Instance._showRankingButton = rankbutton;
   }

    //テクスチャ画像(Down時、UP時)
	public Texture[] playTexture,moreTexture,reviewTexture,exitTexture;

    void Awake()
    {
        //RankingManager.Instance._showRankingButton = rankbutton;
        DialogManager.Instance.SetLabel("Yes", "No", "Close");
    }

	void Start () {
     
        TitleMove.PointNo = 0;
     //   RankingManager.Instance.Content = Content;
   //    PlayerPrefs.SetInt("Cumulative", 0);
       // Debug.Log("Coin:" + PlayerPrefs.GetInt("Cumulative"));
        //rankingPanel = RankingManager.Instance._rankingCanvas;
        BannerDontDestroy.Instance.Dammy();
        if (PlayerPrefs.GetInt("MyAccount") == 1 || Debug.isDebugBuild)
        {
            
            ui.GetComponent<TitleMove>().enabled = true;
           
            NendMenyManager.Instance.AdmobBannerView(true);
            NendMenyManager.Instance.NendIconView(true);
            NendMenyManager.Instance.NendBannerView(true);
            NendMenyManager.Instance.NendRecView(false);
            StartCoroutine("ShowAd");
            
        }
		Time.timeScale = 1;
     //   NendAdInterstitial.Instance.Show();
        /*
		//ios store won't accepect exit buttons 
		#if UNITY_IPHONE
		Destroy(exit);
		#endif
        */
        MyAppleText.text = PlayerPrefs.GetInt("Cumulative").ToString();
        
      
	}

    IEnumerator ShowAd()
    {
        yield return new WaitForSeconds(1);
        if (UnityEngine.Random.value < 0.33f)
        {
            NendAdInterstitial.Instance.Show();
        }
    }
	
	// Update is called once per frame
	void Update ()
    {


        if (showFlag)
        {
            rankingPanel.SetActive(false);
            if (!bannerFlag)
            {
                NendMenyManager.Instance.AdmobBannerView(true);
                NendMenyManager.Instance.NendBannerView(true);
                bannerFlag = true;
            }
        }
        else
        {
            bannerFlag = false;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {

//
// YES NO ダイアログ
            DialogManager.Instance.ShowSelectDialog("終了しますか？", (bool result) => { if (result)Application.Quit(); });
           
        }
        //Down時
		if(Input.GetKeyDown(KeyCode.Mouse0))
		{
           // NendMenyManager.Instance.NendResume();
            hitObjName = "";
			Ray rayObj = uiCam.ScreenPointToRay(Input.mousePosition);
			RaycastHit hitObject;
			if(Physics.Raycast(rayObj,out hitObject))
			{
                //クリックして当たったオブジェクトネーム取得
				hitObjName=hitObject.collider.name;
			}
			switch(hitObjName)
			{
			case "Play":
				SoundController.Static.PlayClickSound();
                NendMenyManager.Instance.NendIconView(false);
				playRender.material.mainTexture=playTexture[1];
				//Title.buttonTween = tweens.Tweenbuttons.slideToLeftSide;
				//iTween.MoveTo(levels,new Vector3(0,0,0),2f);
				//iTween.MoveTo(ui,new Vector3(-29,0,0),2f);
				break;
			case "More":
				SoundController.Static.PlayClickSound();
				moreRender.material.mainTexture=moreTexture[1];
				//Application.OpenURL("");
				break;
			case "Review":
				SoundController.Static.PlayClickSound();
				reviewRender.material.mainTexture=reviewTexture[1];
				//Application.OpenURL("");
				break;
			case "Exit":
				SoundController.Static.PlayClickSound();
				exitRender.material.mainTexture=exitTexture[1];
				Application.Quit();
				break;
			case "Credits":
				SoundController.Static.PlayClickSound();
				break;
            case "Ranking":
              
                //RankingManager.Instance.GetRankingData();
                rankingPanel.SetActive(true);
                NendMenyManager.Instance.NendIconView(false);
                NendMenyManager.Instance.NendBannerView(false);
                NendMenyManager.Instance.AdmobBannerView(false);
           
                break;
            case "Send":
                if (PlayerPrefs.GetInt("MyAccount") == 1)
                {
                    //スコア送信
                    //RankingManager.Instance.SendScore( PlayerPrefs.GetInt("Cumulative"));

                   // RankingManager.Instance.GetMyRankingDate();
                    //RankingManager.Instance.GetRankingData();

                   // StartCoroutine(RankingCheaker());
                    i = 0;
                }
                break;
            case "Add":
                PlayerPrefs.SetInt("Cumulative",50);
              //  Mypoint.text = PlayerPrefs.GetInt("Cumulative").ToString();
                break;
            case "On":
                NendMenyManager.Instance.NendIconView(true);
                NendMenyManager.Instance.NendBannerView(true);
                NendMenyManager.Instance.AdmobBannerView(true);
                break;
                case "Off":
                NendMenyManager.Instance.NendIconView(false);
                NendMenyManager.Instance.NendBannerView(false);
                NendMenyManager.Instance.AdmobBannerView(false);
                break;
			}
		}

        //UP時
		if(Input.GetKeyUp(KeyCode.Mouse0))
		 {
             hitObjName = "";
			Ray rayObj = uiCam.ScreenPointToRay(Input.mousePosition);
			RaycastHit hitObject;
			if(Physics.Raycast(rayObj,out hitObject))
			{
				hitObjName=hitObject.collider.name;
			}
			originalTextures();
			switch(hitObjName)
			{
			case "Play":
				playRender.material.mainTexture=playTexture[0];
				iTween.MoveTo(levels,new Vector3(-0.35f,0,0),2f);
				iTween.MoveTo(ui,new Vector3(-29,0,0),2f);
				break;
			case "More":
				moreRender.material.mainTexture=moreTexture[0];
				Application.OpenURL("");
				break;
			case "Review":
				reviewRender.material.mainTexture=reviewTexture[0];
				Application.OpenURL("");
				break;
			case "Exit":
				exitRender.material.mainTexture=exitTexture[0];
				Application.Quit();
				break;
			case "Credits":
				iTween.MoveTo(creditsDetails,new Vector3(0,1.2f,0),2f);
				iTween.MoveTo(ui,new Vector3(29,0,0),2f);
				
				break;
				
				
			}
		}
	}


    int i = 0;
    private IEnumerator RankingCheaker()
    {


        //ランキング取得できたら
        while (PlayerPrefs.GetString("MYR") == "" && i < 1)
        {
            MyRank.text = "読み込み中";
            yield return new WaitForSeconds(1f);
        }


        yield return new WaitForSeconds(0.5f);

        string myrank = PlayerPrefs.GetString("MYR");
        MyRank.text = myrank + "位";
        i++;

        PlayerPrefs.SetString("MYR", "");

        if (i < 2)
        {
            //RankingManager.Instance.GetRankingData();
            StartCoroutine(RankingCheaker());

          //  RankingManager.Instance.flg = false;
        }
    }


	public void originalTextures()
	{
		#if !UNITY_IPHONE
		exitRender.material.mainTexture=exitTexture[0];
		#endif

		reviewRender.material.mainTexture=reviewTexture[0];
		moreRender.material.mainTexture=moreTexture[0];
		playRender.material.mainTexture=playTexture[0];
	}

    public void OnClickPlayButton()
    {
        showFlag = true;
        SoundController.Static.PlayClickSound();
        NendMenyManager.Instance.NendIconView(false);
       // playRender.material.mainTexture = playTexture[1];
       // playRender.material.mainTexture = playTexture[0];
        iTween.MoveTo(levels, new Vector3(-0.35f, 0, 0), 2f);
        rankingPanel.SetActive(false);
      // iTween.MoveTo(ui, new Vector3(-29, 0, 0), 2f);
       // ui.SetActive(false);
        TitleMove.PointNo = 1;
    }

    public void OnClickRankingOpen() 
    {
        SoundController.Static.PlayClickSound();
        //nkingManager.Instance.GetRankingData();
        rankingPanel.SetActive(true);
        NendMenyManager.Instance.NendIconView(false);
        NendMenyManager.Instance.NendBannerView(false);
        NendMenyManager.Instance.AdmobBannerView(false);
    }
}
