﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;

public class DFPBanner : MonoBehaviour {

    BannerView bannerView;
    AdRequest request;
    private static bool created = false;

    void Awake()
    {
        if (!created)
        {
            // this is the first instance -make it persist
            DontDestroyOnLoad(this.gameObject);
            created = true;
        }
        else
        {
            // this must be aduplicate from a scene reload  - DESTROY!
            Destroy(this.gameObject);
        }
    }

    void Start()
    {
        RequestBanner();
    }

private void RequestBanner()
{
    #if UNITY_ANDROID
    string adUnitId = "/17192736/CN_GameAPP_13_header";
    #elif UNITY_IPHONE
        string adUnitId = "INSERT_IOS_BANNER_AD_UNIT_ID_HERE";
    #else
        string adUnitId = "unexpected_platform";
    #endif

    // Create a 320x50 banner at the top of the screen.
    bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Top);
    // Create an empty ad request.
    request = new AdRequest.Builder().Build();
    // Load the banner with the request.
    bannerView.LoadAd(request);
}

public void Show()
{
    bannerView.Show();
}

public void Hide()
{
    bannerView.Hide();
}
}
