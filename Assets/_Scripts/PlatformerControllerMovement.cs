﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlatformerControllerMovement : MonoBehaviour {

	// The speed when walking
    //歩く時のスピード
	public float walkSpeed= 3.0f;

	// when pressing "Fire1" button (control) we start running

	public float runSpeed= 10.0f;
	
	public float inAirControlAcceleration= 1.0f;
	
	// The gravity for the character
    //文字の為の重力
	public float gravity= 60.0f;
	public float maxFallSpeed= 20.0f;
	
	// How fast does the character change speeds?  Higher is faster.
    //文字はどれほど速くスピードを変更するか？ より高く より速い 。
	public float speedSmoothing= 5.0f;
	
	// This controls how fast the graphics of the character "turn around" when the player turns around using the controls.
    //
	public float rotationSmoothing= 10.0f;
	
	// The current move direction in x-y.  This will always been (1,0,0) or (-1,0,0)
	// The next line,  , tells Unity to not serialize the variable or show it in the inspector view.  Very handy for organization!
	
	public 		Vector3 direction= Vector3.zero;
	
	// The current vertical speed
    //現在の垂直のスピード
	public float verticalSpeed= 0.0f;
	
	// The current movement speed.  This gets smoothed by speedSmoothing.
    //現在の動きスピード。 これは、speedSmoothingすることによってなめらかになる。
	public float speed= 0.0f;
	
	// Is the user pressing the left or right movement keys?
    //ユーザーは左または右の動きキーを押しているか？
	public 		bool isMoving= false;
	
	// The last collision flags returned from controller.Move
    //最後の衝突フラグはcontroller.Moveから返った
	public 	CollisionFlags collisionFlags; 
	
	// We will keep track of an approximation of the character's current velocity, so that we return it from GetVelocity () for our camera to use for prediction.
	
	public 	Vector3 velocity;
	
	// This keeps track of our current velocity while we're not grounded?
	
	public 	Vector3 inAirVelocity= Vector3.zero;
	
	// This will keep track of how long we have we been in the air (not grounded)
	
	public float hangTime= 0.0f;

    void Start()
    {
    //    if (SceneManager.GetActiveScene().buildIndex % 2 == 0)
     //  {
            walkSpeed = 12;
    //    }
    //    else
      //  {
      //      walkSpeed = 6;
      //  }
    }
}
