﻿using UnityEngine;
using System.Collections;

public class takeScreenShot : MonoBehaviour
{

		// Use this for initialization
		void Start ()
		{
				//DontDestroyOnLoad (gameObject);
		}

		string resolution ;
		// Update is called once per frame
		void Update ()
		{

            if (Input.GetKeyDown(KeyCode.G) || Input.GetKeyDown(KeyCode.F10))
            {
                resolution = "" + Screen.width + "X" + Screen.height;

                //PNGファイルとして filename のパスでスクリーンショットを撮ります。
                Application.CaptureScreenshot("ScreenShot-" + resolution + "-" + PlayerPrefs.GetInt("number", 0) + ".png");

                //ゲームデータのセーブ、ロードなどを行います。
                PlayerPrefs.SetInt("number", PlayerPrefs.GetInt("number", 0) + 1);
                //Debug.Log ("takenShot with " + resolution);

            }
	
		}
}
