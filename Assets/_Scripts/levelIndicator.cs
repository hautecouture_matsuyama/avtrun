﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class levelIndicator : MonoBehaviour {

	// Use this for initialization
	public Text levelIndicatorTxt;
	void Start ()
    {
        levelIndicatorTxt = GameObject.Find("LevelText").GetComponent<Text>();
		levelIndicatorTxt.text = Application.loadedLevelName;
	
	}
}
