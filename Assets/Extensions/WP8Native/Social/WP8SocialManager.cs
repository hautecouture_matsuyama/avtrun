﻿#define SA_DEBUG_MODE
using UnityEngine;
using System.Collections;
using UnionAssets.FLE;

public class WP8SocialManager : WPN_Singletone<WP8SocialManager>
{
	void Awake() {
		DontDestroyOnLoad (gameObject);
	}

    public void FacebookPost(string text)
    {
        FacebookPost(text, null);
    }

    public void FacebookPost(string text, Texture2D texture)
    {
        if (texture == null)
        {
        }
    }

    public void TwitterPost(string text)
    {
        TwitterPost(text, null);
    }


    public void TwitterPost(string text, Texture2D texture)
    {
        if (texture == null)
        {
        }

    }

    public void ShareMedia(string text)
    {
        ShareMedia(text, null);
    }

    public void ShareMedia(string text, Texture2D texture)
    {

    }

    public void SendMail(string subject, string body, string recipients)
    {
        SendMail(subject, body, recipients, null);
    }

    public void SendMail(string subject, string body, string recipients, Texture2D texture)
    {
        if (texture == null)
        {
#if (UNITY_WP8 && !UNITY_EDITOR) 
            WP8Native.Social.SendMail(subject, body, recipients);
#endif
        }
        else
        {

        }
    }
}
