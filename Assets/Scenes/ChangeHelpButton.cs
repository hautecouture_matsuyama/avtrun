﻿using UnityEngine;
using System.Collections;

public class ChangeHelpButton : MonoBehaviour {

    public GameObject HelpRanking;
    public GameObject HelpName;

    public void ChangeRanking()
    {
        if(!HelpRanking.active)
        {
            HelpRanking.active = true;
            HelpName.active = false;
        }
    }

    public void ChangeName()
    {
        if (!HelpName.active)
        {
            HelpName.active = true;
            HelpRanking.active = false;
        }
    }

}
