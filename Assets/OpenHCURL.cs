﻿using UnityEngine;
using System.Collections;

public class OpenHCURL : MonoBehaviour {

    private string link;

    public void HCLinks()
    {
        SoundController.Static.PlayClickSound();
#if UNITY_ANDROID
        link = "https://play.google.com/store/apps/developer?id=Hautecouture+Inc.";

#elif UNITY_IPHONE
    link = "https://itunes.apple.com/us/developer/hautecouture-inc./id901721933";
#endif

        Application.OpenURL(link);

    }
}
