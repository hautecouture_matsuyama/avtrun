﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Text;

public class ChangeNameEdit : MonoBehaviour {

    public Text nameText;
    public GameObject HelpCanvas;
    public GameObject RankingCanvas;
    public GameObject ErrPanel;

    public void SetName()
    {
        string strAfter = "";

        //入力されているか？
        if (nameText.text == "")
            return;
        if (!IsKana(nameText.text))
        {
            ErrPanel.active = true;
            return;
        }

        if (nameText.text.Length > 5) strAfter = nameText.text.Remove(5);

        else strAfter = nameText.text;

       // RankingManager.Instance.ChangeName(strAfter);
        
        HelpCanvas.active = false;
        RankingCanvas.active = false;
       
    }

    public static bool IsKana(string target)
    {
        foreach (var chara in target)
        {
            var charaData = Encoding.UTF8.GetBytes(chara.ToString());

            if (charaData.Length != 3)
            {
                return false;
            }

            var charaInt = (charaData[0] << 16) + (charaData[1] << 8) + charaData[2];

            if (charaInt < 0xe38181 || charaInt > 0xe38293)
            {
                return false;
            }
        }

        return true;
    }


}
