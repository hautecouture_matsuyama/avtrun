﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CloseButton : MonoBehaviour {

    public GameObject RankingCanvas;
    [SerializeField]
    GameObject[] buttonsUI = new GameObject[0];

    public void CloseRanking()
    {
        RankingCanvas.SetActive(false);

    }

    public void CloseTitleRanking()
    {
        SoundController.Static.PlayClickSound();

        /*for (int i = 0; i < buttonsUI.Length; i++)
        {
            buttonsUI[i].SetActive(false);
        }*/
        NendMenyManager.Instance.NendIconView(true);
        NendMenyManager.Instance.NendBannerView(true);
        NendMenyManager.Instance.AdmobBannerView(true);
        RankingCanvas.SetActive(false);
    }
}
