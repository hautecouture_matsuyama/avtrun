﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using NendUnityPlugin.AD;

public class NendMenyManager : MonoBehaviour {
    [SerializeField]
    GameObject BannerObs;
    [SerializeField]
    GameObject Rec;
    [SerializeField]
    GameObject Rec2;
    [SerializeField]
    GameObject Icon;

    [SerializeField]
    GameObject OverIcon;
    [SerializeField]
    GameObject ResultIcon;
    [SerializeField]
    GameObject AdMobBannerobs;
    
   public int SceneNo = 0;//0,title  1,Play   2,GameOver

    //シングルトン
    private static NendMenyManager _instance = null;


    void Awake()
    {


        if (_instance == null)
        {

            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {

            Destroy(gameObject);
        }
    }

    public static NendMenyManager Instance
    {
        get
        {
            return _instance;
        }
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}



     public void NendBannerView(bool viweFlag)
    {
        
        if (viweFlag)
        {
            BannerObs.GetComponent<NendAdBanner>().Show();
        
        }
        else
        {
            BannerObs.GetComponent<NendAdBanner>().Hide();
           
        }
       
       
    }

     public void NendRecView(bool viweFlag)
     {
         
         if (viweFlag)
         {
             Rec.GetComponent<NendAdBanner>().Show();
            
         }
         else
         {
             Rec.GetComponent<NendAdBanner>().Hide();
         
         }
         
  
     }


     public void NendRec2View(bool viweFlag)
     {
         if (viweFlag)
         {
             Rec2.GetComponent<NendAdBanner>().Show();
         
         }
         else
         {
             Rec2.GetComponent<NendAdBanner>().Hide();
           
         }
        
     }

     public void NendIconView(bool viweFlag)
     {
         if (viweFlag)
         {
             //Icon.GetComponent<NendAdIcon>().Show();
         }
         else
         {
             //Icon.GetComponent<NendAdIcon>().Hide();
         }
     }


     public void NendOverIconView(bool viweFlag)
     {
         if (viweFlag)
         {
             OverIcon.GetComponent<NendAdIcon>().Show();
         }
         else
         {
             OverIcon.GetComponent<NendAdIcon>().Hide();
         }
     }

     public void NendResultIconView(bool viweFlag)
     {
         if (viweFlag)
         {
             ResultIcon.GetComponent<NendAdIcon>().Show();
         }
         else
         {
             ResultIcon.GetComponent<NendAdIcon>().Hide();
         }
     }

     public void AdmobBannerView(bool viweFlag)
     {
         if (viweFlag)
         {
             
            /// AdMobBannerobs.GetComponent<AndroidAdMobBanner>().anchor = TextAnchor.UpperCenter;
             AdMobBannerobs.GetComponent<DFPBanner>().Show();
         }
         else
         {
           //  AdMobBannerobs.GetComponent<AndroidAdMobBanner>().anchor = TextAnchor.LowerCenter;
             //AdMobBannerobs.GetComponent<AndroidAdMobBanner>().ShowBanner();
             AdMobBannerobs.GetComponent<DFPBanner>().Hide();
         }
     }

     public void NendResume()
     {
        // BannerObs.GetComponent<NendAdBanner>().Resume();
        // Rec.GetComponent<NendAdBanner>().Resume();
         //Rec2.GetComponent<NendAdBanner>().Resume();
     }



    

     public string GetRecName()
     {
         return Rec.GetComponent<NendAdBanner>().name;
     }
}
