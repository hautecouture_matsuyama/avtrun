﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Facebook.Unity.Example;

public class SendMethodScript : MonoBehaviour {
    [SerializeField]
    GameObject managerObject;

    [SerializeField]
    Button bt;
    public SoundController audioMngr;
    GameObject faceobject;

    void Awake()
    {
        
    }

	// Use this for initialization
    void Start()
    {
        audioMngr = GameObject.Find("SoundController").GetComponent<SoundController>();
        managerObject = GameObject.Find("UiContainer");
        faceobject = GameObject.Find("FaceBook");
        gameObject.GetComponent<Button>().onClick.AddListener(OnClickEvent);
        bt = gameObject.GetComponent<Button>();

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnClickEvent()
    {
        audioMngr.PlayClickSound();
        if (gameObject.name == "Next")
        {
            managerObject.GetComponent<IngameUI>().OnClickNextLevelButton();
        }
        if (gameObject.name == "Retry")
        {
            managerObject.GetComponent<IngameUI>().OnClickRetryButton();
        }
        if (gameObject.name == "Home")
        {
            managerObject.GetComponent<IngameUI>().OnClickHomeButton();
        }

        if (gameObject.name == "Twitter")
        {
            managerObject.GetComponent<IngameUI>().OnClickTwitterButton();
        }
        if (gameObject.name == "facebookbtn")
        {
            Debug.Log("FaceBook");
            faceobject.GetComponent<MainMenu>().Login();
        }
        if (gameObject.name == "LINE")
        {
            managerObject.GetComponent<IngameUI>().OnClickLINEButton();
        }
        if (gameObject.name == "Pause")
        {
            managerObject.GetComponent<IngameUI>().PauseButton();
        }
    }
}
