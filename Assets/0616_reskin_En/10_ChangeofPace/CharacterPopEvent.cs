﻿using UnityEngine;
using System.Collections;

public class CharacterPopEvent : MonoBehaviour {

    [SerializeField]
    GameObject[] characterObs = new GameObject[0];

    [SerializeField]
    GameObject secretCharacter;
    int rand;
    [SerializeField]
    int maxRand;
    PlatformerControllerMovement player;


    float randomNum;

	// Use this for initialization
	void Start () {
        player = GameObject.Find("player").GetComponent<PlatformerControllerMovement>();
        CollisionAndTrigger.moveFlag = false;
        StartCoroutine("ActiveCharacter");
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    IEnumerator ActiveCharacter()
    {
        randomNum = Random.value;

        rand = Random.Range(0, characterObs.Length);
        if (BgScroll.ScrollFlg && player.velocity.x != 0)
        {
            if (randomNum < 0.05f)
            {
                characterObs[rand].SetActive(true);
            }
            else if (randomNum <=0.01f)
            {
                secretCharacter.SetActive(true);
            }
        }
        yield return new WaitForSeconds(2);
        if (!CollisionAndTrigger.moveFlag)StartCoroutine("ActiveCharacter");
    }
}
