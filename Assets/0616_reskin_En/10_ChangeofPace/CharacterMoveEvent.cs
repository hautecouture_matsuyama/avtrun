﻿using UnityEngine;
using System.Collections;

public class CharacterMoveEvent : MonoBehaviour {

    float speed = 1f;//画像の移動スピード最背面の背景と同じ速度で移動
    bool showFlag = false;//tureの際にしか画像の非表示・初期化を行わないフラグ
    PlatformerControllerMovement player;

	// Use this for initialization
	void Start () {
        //書くシーン
        player = GameObject.Find("player").GetComponent<PlatformerControllerMovement>();
	}
	
	// Update is called once per frame
	void Update () {
        if (player.velocity.x != 0 && BgScroll.ScrollFlg)
        {
            transform.Translate(-speed * Time.deltaTime, 0, 0);

        }
	}


    void OnBecameInvisible()
    {
        if (showFlag)
        {
            Debug.Log("isCall");
            showFlag = false;
            gameObject.transform.localPosition = new Vector3(10.17f, 3.065194f, 18.25293f);
            gameObject.SetActive(false);
        }
    }

    void OnBecameVisible()
    {
        showFlag = true;
    }

  /*  void OnDisable()
    {
        gameObject.transform.localPosition = new Vector3(10.17f,3.065194f,18.25293f);
        showFlag = false;
        gameObject.SetActive(true);
    }*/
}
